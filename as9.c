#include <stdio.h>
#define MAX_CHAR_SIZE 100

int main()
{
    FILE *fileP, *fileQ, *fileR;
    char content[MAX_CHAR_SIZE];

    fileP = fopen("assignment9.txt", "w");
    fprintf(fileP, "UCSC is one of the leading institutes in Sri Lanka for computing studies.");
    fclose(fileP);

    fileQ = fopen("assignment9.txt", "r");
    while(!feof(fileQ))
    {
        fgets(content, MAX_CHAR_SIZE, fileQ);
        puts(content);
    }
    fclose(fileQ);

    fileR = fopen("assignment9.txt", "a");
    fprintf(fileR, "\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
    fclose(fileR);

    return 0;
}