#include<stdio.h>
 
int prime(int);
 
int main()
{
   int n, result;
   printf("Enter an integer: \n");
   scanf("%d",&n);
 
   result = prime(n);
 
   if ( result == 1 )
      printf("%d is prime.\n", n);
   else
      printf("%d is not prime.\n", n);
 
   return 0;
}
 
int prime(int n)
{
   int c;
 
   for ( c = 2 ; c <= n - 1 ; c++ )
   { 
      if ( n%c == 0 )
     return 0;
   }
   return 1;
}
