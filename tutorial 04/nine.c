#include <stdio.h>
 
int Armstrong(int num);
void printArmstrong(int n1, int n2);

int main()
{
    int n1, n2;
    
    printf("Enter number interval (from to): ");
    scanf("%d %d", &n1, &n2);
    
    printf("All armstrong numbers between %d to %d are: \n", n1, n2);
    printArmstrong(n1, n2);
    
    return 0;
}

int Armstrong(int num)
{
    int temp, lastDigit, sum;
    
    temp = num;
    sum = 0;
    
    // sum of cube of digits
    while(temp != 0)
    {
        lastDigit = temp % 10;
        sum += lastDigit * lastDigit * lastDigit;
        temp /= 10;
    }
    
    
     //Check if sum of cube of digits equals to original number.
     if(num == sum)
        return 1;
    else 
        return 0;
}

//Print all armstrong numbers between start and end. 
void printArmstrong(int n1, int n2)
{
    while(n1 <= n2)
    {
        if(Armstrong(n1)) 
        {
            printf("%d, ", n1);
        }
        
        n1++;
    }
}