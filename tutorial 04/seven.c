#include <stdio.h>
int Prime(int n);

int main() {
    int n1, n2, i, flag;
    printf("Enter number interval (from to): ");
    scanf("%d %d", &n1, &n2);
    printf("Prime numbers between %d and %d are: ", n1, n2);

    for (i = n1 + 1; i < n2; ++i) {
        flag = Prime(i);
        if (flag == 1)
        printf("%d ", i);
    }
    return 0;
}

int Prime(int n) {
    int c, flag = 1;
    for (c = 2; c <= n / 2; ++c) {
        if (n % c == 0) {
            flag = 0;
            break;
        }
    }
    return flag;
}