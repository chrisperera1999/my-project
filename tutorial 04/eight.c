#include <stdio.h>

long long fact(int num);
void strong(int n1, int n2);


int main()
{
    int n1, n2;
    printf("Enter number interval (from to): ");
    scanf("%d %d", &n1, &n2);
    printf("All strong numbers between %d to %d are: \n", n1, n2);
    strong(n1,n2);
    return 0;
}

void strong(int n1, int n2)
{
    long sum;
    int num;
    
    while(n1 != n2)
    {
        sum = 0;
        num = n1;
        
        // Calculate sum of factorial of digits
        while(num != 0)
        {
            sum += fact(num % 10);
            num /= 10; 
        }

        // If sum of factorial of digits equal to current number
        if(n1 == sum)
        {
            printf("%d, ", n1);
        }
        n1++;
    }
}

long long fact(int num)
{
    if(num == 0)
        return 1;
    else
        return (num * fact(num-1));
}