#include<stdio.h>
const float PI = 3.1415927;
float area(float);
float circum(float);
float diam(float);

int main() {
 float r;
 printf("Enter radius: ");
 scanf("%f", &r);
 printf("Area : %.3f\n", area(r));
 printf("Circumference: %.3f\n", circum(r));
 printf("diameter: %.3f\n", diam(r));
}
// area  
float area(float r) {
  return PI * r * r;
}
// circumference 
float circum(float r) {
  return 2 * PI * r;
}
// diameter
float diam(float r){
  return r*2;
}