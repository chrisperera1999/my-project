#include <stdio.h>
int getProfit(int,int);
int getRevnue(int,int);
int getCost(int);
int getAttendees(int);

 int getRevenue(int ticket,int attendees){
    int revenue = ticket*attendees;
    return revenue;
}
int getCost(int attendees){
    int cost=500+attendees*3;
    return cost;
}
int getProfit(int revenue,int cost){
   int profit= revenue-cost;
    return profit; 
}
int getAttendees(int ticket){
//180 is the max attendees (when ticket price is 0, no.of attendees is 180)
 int attendees = 180-4*ticket;
 return attendees;
}
int main(){
    int ticket=0,attendees=0,profit=-1040,maxprofit=-2000;
    //minimum profit(loss) is -1040 (when ticket price is 0)
   while (profit>maxprofit){
    maxprofit = profit;
    ticket=ticket+5;
    attendees = getAttendees(ticket);
    profit = getProfit(getRevenue(ticket,attendees),getCost(attendees));
    printf("Profit when ticket price is Rs.%d : %d\n",ticket,profit);    
   }
   printf("Max profit is when ticket price is Rs.%d : %d\n",ticket-5,maxprofit);
   
    return 0;
}