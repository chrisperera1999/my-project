#include <stdio.h>
void pattern(int x); 
void row(int y);
int rows=1; 
int c;

void pattern(int x)
{  
    if(x>0)
    {
        row(rows);
        printf("\n");
        rows++;
        pattern(x-1);
        
    }
}

void row(int y)
{
    if(y>0)
    {
        printf("%d",y);
        row(y-1);
    }
}

int main()
{
    printf("Enter the number of rows needed : ");
    scanf("%d",&c);
    pattern(c);
    return 0;
}
