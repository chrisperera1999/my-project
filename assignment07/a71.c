#include<stdio.h>
void reverse(char *);

int main(){
    char a[100];
    printf("enter sentence to be reversed:  \n");
    scanf("%[^\n]s",a); // [^\n] is to scan the entire input even with spaces
    reverse(a);//calling the recursive function
    return 0;
}
void reverse(char *a){ 
    if (*a){//to traverse till the last character of the array
        reverse(a+1);//to move to the next character
        printf("%c",*a);// to print from the last
    }
}